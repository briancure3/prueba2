#ifndef ICONTROLADORC_H
#define ICONTROLADORC_H
#include <stdlib.h>
#include <iostream>
#include <set>
#include "DtConversacion.h"
#include "Conversacion.h"

class IControladorC {
public:
    IControladorC();
    IControladorC(const IControladorC& orig);
    virtual ~IControladorC();
    virtual Conversacion* altaConversacion(std::string)=0;
    virtual std::set<DtConversacion*> listarConversaciones(std::string)=0;
private:

};

#endif /* ICONTROLADORC_H */

