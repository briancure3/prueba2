#ifndef CONTROLADORC_H
#define CONTROLADORC_H
#include "IControladorC.h"
#include "Sesion.h"
#include "Usuario.h"
#include "DtConversacion.h"
#include "Conversacion.h"

class ControladorC : public IControladorC {
public:
    ControladorC();
    ControladorC(const ControladorC& orig);
    virtual ~ControladorC();
    Conversacion* altaConversacion(std::string);
    std::set<DtConversacion*> listarConversaciones(std::string);
private:
};

#endif /* CONTROLADORC_H */

