#include "Conversacion.h"
unsigned int Conversacion::id_counter=1;

Conversacion::Conversacion() {
  id= Conversacion::id_counter;
   id_counter++;
  
}

Conversacion::Conversacion(const Conversacion& orig) {
}

Conversacion::~Conversacion() {
}

int Conversacion::getId()
{
    return this->id;
}
bool Conversacion::getEsActiva()
{
    return this->esActiva;
}
bool Conversacion::getEsGrupal()
{
    return this->esGrupal;
}
void Conversacion::setId(int i)
{
    (*this).id=i;
}
void Conversacion::setEsActiva(bool act)
{
    (*this).esActiva=act;
}
void Conversacion::setEsGrupal(bool gru)
{
    (*this).esGrupal=gru;
}

void Conversacion::setUsuarios(Usuario* emi, Usuario* rece)
{
    (*this).emisor=emi;
    (*this).receptor=rece;
}

Usuario* Conversacion::getU()
{
    return (*this).receptor;
}

void Conversacion::enviarMensajeSimple(std::string msj, Usuario* u)
{
    Mensaje* m= new MSimple();
    m->setTexto(msj);
    if((*this).esGrupal)
    {
        m->setReceptores(receptoresG);//SOLO PARA GRUPALES
    }
    else
    {
        m->setReceptor((*this).receptor);
    }
}

void Conversacion::enviarMensajeVideo(std::string URL,int Duracion, Usuario* u)
{
    Mensaje* mv= new MVideo();
    mv->setURL(URL);
    mv->setDuracion(Duracion);
    if((*this).esGrupal)
    {
        mv->setReceptores(receptoresG);//SOLO PARA GRUPALES
    }
    else
    {
        mv->setReceptor((*this).receptor);
    }
}