
#ifndef MSIMPLE_H
#define MSIMPLE_H
#include "Mensaje.h"

class MSimple: public Mensaje {
public:
    MSimple();
    MSimple(const MSimple& orig);
    virtual ~MSimple();
    string getTexto();
    void setTexto(string);
    void setReceptores(set<Usuario*>);
    void setReceptor(Usuario*);
private:
    string texto;

};

#endif /* MSIMPLE_H */

