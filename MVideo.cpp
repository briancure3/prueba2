

#include "MVideo.h"

MVideo::MVideo() {
}

MVideo::MVideo(const MVideo& orig) {
}

MVideo::~MVideo() {
}

string MVideo::getURL()
{
    return this->URL;
}

string MVideo::getDuracion()
{
    return this->Duracion;
}

void MVideo::setURL(string url)
{
    (*this).URL=url;
}

void MVideo::setDuracion(int duracion)
{
    (*this).Duracion=duracion;
}

void MVideo::setReceptores(set<Usuario*> rec)
{
    set<Usuario*>::iterator it5;
    for(it5=rec.begin();it5!=rec.end(); ++it5)
    {
        DatosVisto *dv_1= new DatosVisto;
        dv_1->setReceptor(*it5);
        (*this).dv.insert(dv_1);
    }
}

void MVideo::setReceptor(Usuario* rec){
    DatosVisto *dv_2= new DatosVisto;
    dv_2->setReceptor(rec);
}