#include <cstdlib>
#include <stdlib.h>
#include <iostream>
#include "Sesion.h"
#include "Usuario.h"
#include "Fabrica.h"
#include "IControladorU.h"
#include "IControladorG.h"
#include "FechaActual.h"
#include "DtConversacion.h"
#include <string>
#include <set>

void menu();
void abrirGuasapFING();
void cerrarGuasapFING();
void agregarContactos();
void enviarMensaje();
void altaGrupo();
int main(int argc, char** argv) {
    int opc, d, m, a, h, min;
    FechaActual *fechAct= FechaActual::getInstance();
    do{
    menu(); 
    cin >> opc;
    switch(opc)
    {
        case 1:
            abrirGuasapFING();
        break;
        case 2:
            cerrarGuasapFING();
        break;
        case 3:
            agregarContactos();
        break;
        case 4:
            enviarMensaje();
        break;
        case 5: 
            altaGrupo();
        break;
        case 10:
            std::cout<<"Ingrese el dia: ";
            std::cin>>d;
            std::cout<<"Ingrese el mes: ";
            std::cin>>m;
            std::cout<<"Ingrese el anio: ";
            std::cin>>a;
            std::cout<<"Ingrese la hora: ";
            std::cin>>h;
            std::cout<<"Ingrese los minutos: ";
            std::cin>>min;
            if(d>0 && d<=31 && m>0 && m<=12 && a>0 && a<=9999 && h>0 && h<=24 && min>0 && min<=59){
                fechAct= FechaActual::getInstance();
                fechAct->modFechAct(d, m, a, h, min);
                std::cout<<"\nLa fecha del Sistema se modifico correctamente"<<std::endl;
            }
            else
            {
                std::cout<<"\nLa fecha ingresada es incorrecta"<<std::endl;
            }
        break;
        case 11:
            fechAct= FechaActual::getInstance();
            fechAct->verFechAct();
        break;
        case 0:
            
        break;
        default:
            std::cout << "Ingreso una opcion incorrecta " << std::endl;
        break;
    }
        
    }while(opc !=0);
    return 0;
}

void menu()
{
    Sesion *actual= Sesion::getInstance();
    if(actual->getU()!= NULL)
    {
       std::cout << std::endl;
       std::cout<<"ESTADO: CONECTADO - Usuario Actual: " << actual->getU()->getTelefono() <<std::endl;
    }
else
    { 
        std::cout << std::endl;
        std::cout<< "ESTADO: DESCONECTADO" <<  std::endl;
    }
    std::cout <<"1-Abrir GuasapFING" << std::endl;
    std::cout <<"2-Cerrar GuasapFing" << std::endl;
    std::cout <<"3-Agregar Contactos" << std::endl;
    std::cout <<"4-Enviar Mensaje" << std::endl;
    std::cout <<"5-Alta Grupo" << std::endl;
    std::cout <<"10-Modificar fecha del Sistema" << std::endl;
    std::cout <<"11-Consultar fecha del Sistema" << std::endl;
    std::cout <<"0-Salir" << std::endl;
}

void abrirGuasapFING()
{
    string tel, opc, sal;
    Fabrica* fabrica=Fabrica::getInstance();
    IControladorU *interfaz = fabrica->getInterfazU();
    do{
        std::cout<<"Ingrese su telefono" << std::endl;
        std::cin>>tel;        
        opc=interfaz->ingresarNumero(tel);
        if(opc=="No esta")
        {
            int ele;
            std::cout<<"El usuario no existe, presione 1 para darle de alta, caso contrario se cerrara la operacion " << std::endl;
            std::cin>> ele;
            if(ele==1)
            {
                string nom, url, desc;
                std::cout << "Ingrese su nombre: " << std::endl;
                std::cin.ignore();
                std::getline(std::cin, nom);
                std::cout << "Ingrese la url de su imagen: " << std::endl;
                std::getline(std::cin, url);
                std::cout << "Ingrese su descripcion:"  << std::endl;
                std::getline(std::cin, desc);
                interfaz->darAltaNumero(tel,nom,url,desc);
            }
            else
            {
                opc = "Salir";
            }
            std::cout << "Se inicio sesion correctamente" << endl;
        }
        if(opc=="Se logueo")
        {
            std::cout << "Se inicio sesion Correctamente" << std::endl;
        }
        if(opc=="Cerrar")
        {
            int ele;
            std::cout<<"Ya esta logueado, presione 1 para cerrar sesion, caso contrario se cerrara la operacion " << std::endl;
            std::cin>> ele;
            if(ele==1)
            {
                cerrarGuasapFING();
            }
            else
            {
                opc = "Salir";
            }
        }
        if(opc!="Salir")
        {
        std::cout<<"Presione 1 para volver a ingresar un numero, caso contrario saldra de la operacion"<< std::endl;
        std::cin>>sal;
        if(sal!="1")
        {
            opc="Salir"; 
        }
        }
    }while(opc!="Salir");    
    fabrica->borrarInterfaz(interfaz);
}

void cerrarGuasapFING(){
   Fabrica *fabrica=Fabrica::getInstance();
   IControladorU *interfaz = fabrica->getInterfazU(); 
   interfaz->cerrarSesion(); 
    
}

void agregarContactos()
{
   Fabrica *fabrica=Fabrica::getInstance();
   IControladorU *interfaz = fabrica->getInterfazU();
   string opcionS;
   do{
   Sesion *login= Sesion::getInstance();
   if(login->getU()==NULL)
    {
        std::cout<<"Debe iniciar Sesion Primero" << std::endl;
    }
    else
    {
      set<Usuario*> listaC;
      set<Usuario*>::iterator it;
      listaC=interfaz->obtenerContactos();
      if(listaC.empty())
      {
       std::cout<< "Aun no posee contactos" << std::endl;
      }
      else
      {
          std::cout<< "*CONTACTOS*" <<std::endl;
          int contador = 1;
         for(it=listaC.begin(); it!=listaC.end();++it)
         {
          std::cout<< "Contacto Nº " << contador <<  "-> Telefono: " << (*it)->getTelefono()<<  "  Nombre: " << (*it)->getNombre() << "  Url: " << (*it)->getUrl() << std::endl;
          contador ++;
         }  
        
      }
       string opc;
         std::cout << "Presione 1 para agregegar un nuevo contacto, caso contrario la operacion finalizara" << std::endl;
         std::cin>>opc;
         if(opc=="1")
         {
             string tel;
             std::cout<<"Ingrese el telefono del nuevo contacto"<< std::endl;
             cin>>tel;
              if(login->getU()->getTelefono()==tel)
             {
                 std::cout<< "Ingresaste tu propio telefono" << std::endl;
             }
              else
              {
                DtUsuario *dt=interfaz->mostrarContacto(tel);
                if(dt!=NULL)
                {
                    interfaz->confirmar(dt);
                } 
             
              }
             
             
         }
         else
         {
             opcionS="2";
         }
    }
   if(opcionS=="1")
   {
      std::cout << "Presione 1 para continuar agregando, caso contrario se cerrara la operacion" << std::endl;
      cin>>opcionS; 
   }
   }while(opcionS == "1");
   
}
void enviarMensaje()
{
    Sesion *login= Sesion::getInstance();
    
    if(login->getU()==NULL)
    {
        std::cout<<"Debe iniciar Sesion Primero" << std::endl;
    }
    else
    {
    Fabrica* fabrica=Fabrica::getInstance();
    IControladorC *interfazC=fabrica->getInterfazC();
    IControladorU *interfazU=fabrica->getInterfazU();
    set<DtConversacion*> listaConvA=interfazC->listarConversaciones("Activas");
    set<DtConversacion*>::iterator itc;
    set<DtConversacion*> listaConvAr=interfazC->listarConversaciones("Archivadas");
    set<DtConversacion*>::iterator itc2; 
    
    if(listaConvA.empty())
    {
        std::cout<<"Aun no posee conversaciones Activas" <<std::endl;
    }
    else
    {
        std::cout<< "*CONVERSACIONES ACTIVAS: (Simples de momento)*" <<std::endl;
         for(itc=listaConvA.begin(); itc!=listaConvA.end();++itc)
         {
          std::cout<< "Id Conversacion: " << (*itc)->getId() <<  "-> Telefono del receptor: " << (*itc)->getTelefono()<<  "  Nombre del receptor: " << (*itc)->getNombre() << std::endl;
         }  
    }
    int contador=0;
    for(itc2=listaConvAr.begin(); itc2!=listaConvAr.end();++itc2)
    {
        contador++;
    }
    std::cout<< "Archivadas :  " <<contador<< std::endl;
    Conversacion* c;
    int opc,id_c;
    std::cout<< "1- Seleccionar Conversacion Activa" << std::endl;
    std::cout<< "2- Ver las Conversaciones Archivadas" << std::endl;
    std::cout<< "3- Enviar un mensaje a un contacto con el cual aun no ha iniciado una conversacion" << std::endl;
    std::cin>>opc;
    switch (opc){
        case 1:
            std::cout<< "Ingrese la id de la conversacion: " << std::endl;
            cin>>id_c;
            c=(login->getU()->seleccionarConversacion(id_c)); //NO PUEDE VERIFICAR ELLA 
        break;
        case 2:
        {   
            if(listaConvAr.empty())
            {
                std::cout<<"Aun no posee conversaciones Archivadas" <<std::endl;
            }
            else
            {
            std::cout<< "*CONVERSACIONES ARCHIVADAS: (Simples de momento)*" <<std::endl;
            for(itc2=listaConvAr.begin(); itc2!=listaConvAr.end();++itc2)
            {
            std::cout<< "Id Conversacion: " << (*itc2)->getId() <<  "-> Telefono del receptor: " << (*itc2)->getTelefono()<<  "  Nombre del receptor: " << (*itc2)->getNombre() << std::endl;
            }  
            }
            std::cout<< "Ingrese la id de la conversacion archivada: " << std::endl;
            cin>>id_c;
            for(itc2=listaConvAr.begin();itc2!=listaConvAr.end();++itc2)//NO FUNCIONA 
            {
                if((*itc2)->getId()==id_c)
                {
                    c=dynamic_cast<Conversacion*>(*itc2);
                    c->setEsActiva(true);
                }
            }
             //NO FUNCIONA 
        }
        break;
        case 3:
        {
            
           set<Usuario*> listaC=interfazU->obtenerContactos();
           set<Usuario*>::iterator it;
           if(listaC.empty())
           {
                std::cout<< "Aun no posee contactos" << std::endl;
           }
           else
            {
            std::cout<< "*CONTACTOS*" <<std::endl;
            int contador = 1;
            for(it=listaC.begin(); it!=listaC.end();++it)
            {
            std::cout<< "Contacto Nº " << contador <<  "-> Telefono: " << (*it)->getTelefono()<<  "  Nombre: " << (*it)->getNombre() << std::endl;
            contador ++;
            }   
            }
            std::string tel;///ESTO DEBE IR LUEGO DEL MOSTRAR
            std::cout<< "Ingrese el numero del contacto " << std::endl;
            cin>>tel;
            c=interfazC->altaConversacion(tel);
        }
        break;
        default:
        break;
    }
    if(c!=NULL)
    {
        //AQUI DEBE IR SWITCH CON LOS DISTINTOS TIPOS DE ENVIAR MENSAJE////////////////////////////////////////////////////////////////////////////////////////////////
        string msj;
        std::cout<<"Ingrese el texto del mensaje Simple: " << std::endl;
        std::cin.ignore();
        std::getline(std::cin,msj);
        c->enviarMensajeSimple(msj, login->getU());
    }
    
    
    
    // interfazC->altaConversacion("15");
     // interfazC->altaConversacion("20");
    }
}

void altaGrupo(){
    Fabrica* fabrica=Fabrica::getInstance();
    IControladorG *interfaz = fabrica->getInterfazG();
    IControladorU *interfazU = fabrica->getInterfazU();
    Sesion *login= Sesion::getInstance();
    
    if(login->getU()==NULL)
    {
        std::cout<<"Debe iniciar Sesion Primero" << std::endl;
    }
    else
    {
        set<Usuario*> contactos;
        set<Usuario*>::iterator it3;
        contactos=interfazU->obtenerContactos();
        if(contactos.empty()){
            std::cout<<"No se tiene contactos"<<std::endl;
        }
        else
        {
            int opcion, opt;
            string telef, nombreG, urlG;
            bool esta = false;
            bool siAltaG=false;
            Usuario* user=login->getU();
            set<DtContacto*> elegidos;
            set<DtContacto*> restantes;
            set<DtContacto*>::iterator it = elegidos.begin();
            set<DtContacto*>::iterator it2, it4 = restantes.begin();
            
            
            for(it3=contactos.begin(); it3!=contactos.end();++it3)
            {
                DtContacto* cont=new DtContacto;
                cont->setTelefono((*it3)->getTelefono());
                cont->setNombre((*it3)->getNombre());
                restantes.insert(cont);
            }
            
            interfaz->setNoElegidos(restantes);
            do{

                elegidos=interfaz->listarElegidos();
                it=elegidos.begin();
                restantes=interfaz->listarNoElegidos();
                it2=restantes.begin();
                
                if(elegidos.empty()){
                    std::cout<<"No hay contactos elegidos\n"<<std::endl;
                }
                else
                {
                    std::cout<<"Lista de Contactos elegidos:\n"<<std::endl;
                    while(it != elegidos.end() ){
                        std::cout<<"Telefono: "<< (*it)->getTelefono()<<"  "<<"Nombre: "<< (*it)->getNombre() <<std::endl;
                        ++it;
                    }
                }
                
                if(restantes.empty()){
                    std::cout<<"No hay contactos disponibles para elegir\n"<<std::endl;
                }
                else
                {
                    std::cout<<"Lista de Contactos disponibles para elegir:\n"<<std::endl;
                    while(it2 != restantes.end() ){
                        std::cout<<"Telefono: "<< (*it2)->getTelefono()<<"  "<<"Nombre: "<< (*it2)->getNombre() <<std::endl;
                        ++it2;
                    }
                }
                
                std::cout<<"\nIngrese el numero de la opcion que desee realizar: \n"<<std::endl;
                std::cout<<"1- Si desea elegir un contacto como participante"<<std::endl;
                std::cout<<"2- Si desea quitar un contacto como participante"<<std::endl;
                std::cout<<"3- Si desea dejar de elegir participantes y crear el grupo"<<std::endl;
                std::cout<<"0- Si desea cancelar la operacion"<<std::endl;
                std::cin>>opcion;
                
                switch(opcion)
                {
                    case 1:
                        std::cout<<"Ingrese el telefono del contacto: ";
                        std::cin>>telef;
                        if(user->esContacto(telef)){
                            if(restantes.empty()){
                                std::cout<<"No quedan contactos para seleccionar\n"<<std::endl;
                            }
                            else
                            {
                                it2=restantes.begin();
                                while(it2 != restantes.end() || esta!=true){
                                    if((*it2)->getTelefono()==telef){
                                        esta=true;
                                    }
                                    it2++;
                                }
                                if(esta==true){
                                    interfaz->agregarParticipante(telef);
                                    esta=false;
                                }
                                else
                                {
                                    std::cout<<"El contacto ya fue elegido\n"<<std::endl;
                                }
                            }
                        }
                        else
                        {
                            std::cout<<"El telefono ingresado no es un contacto, intentelo nuevamente\n"<<std::endl;
                        }
                    break;
                    case 2:
                        std::cout<<"Ingrese el telefono del contacto: ";
                        std::cin>>telef;
                        if(user->esContacto(telef)){
                            if(elegidos.empty()==false){
                                it=elegidos.begin();
                                while(it != elegidos.end() || esta!=true){
                                    if((*it)->getTelefono()==telef){
                                        esta=true;
                                    }
                                    it++;
                                }
                                if(esta==true){
                                    interfaz->borrarParticipante(telef);
                                    esta=false;
                                }
                                else
                                {
                                    std::cout<<"El contacto no se puede quitar porque no fue elegido\n"<<std::endl;
                                }
                            }
                        }
                        else
                        {
                            std::cout<<"El telefono ingresado no es un contacto, intentelo nuevamente\n"<<std::endl;
                        }
                    break;
                    case 3:
                        if(elegidos.empty()){
                            do{
                                std::cout<<"Debe elegir al menos un participante\n"<<std::endl;
                                std::cout<<"Ingrese 1 si desea intentar nuevamente el proceso de eleccion de participantes"<<std::endl;
                                std::cout<<"Ingrese 0 si desea cancelar la operacion"<<std::endl;
                                cin >> opt;
                                if(opt==1){
                                    opt=0;
                                }
                                else
                                {
                                    if(opt==0){
                                        opcion=0;
                                    }
                                    else
                                    {
                                        std::cout<<"Ingreso una opcion incorrecta, intentelo nuevamente\n"<<std::endl;
                                    }
                                }
                            }while(opt!=0);
                        }
                        else
                        {
                            opcion=0;
                            siAltaG=true;
                        }
                    break;
                    case 0:
                        
                    break;
                    default:
                        std::cout<<"Ingreso una opcion incorrecta, intentelo nuevamente\n"<<std::endl;
                    break;
                }
            }while(opcion!=0);
            
            if(siAltaG==true){
                std::cout<<"Ingrese el nombre del grupo: ";
                std::cin>>nombreG;
                std::cout<<"Ingrese la url de la imagen del grupo: ";
                std::cin>>urlG;
                interfaz->darAltaGrupo(nombreG, urlG);
            }
            restantes.clear();
            elegidos.clear();
        }
        
    }
    
}