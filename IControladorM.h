#ifndef ICONTROLADORM_H
#define ICONTROLADORM_H
#include <iostream>
#include <stdlib.h>
#include "Conversacion.h"

class IControladorM {
public:
    IControladorM();
    IControladorM(const IControladorM& orig);
    virtual ~IControladorM();
    virtual void enviarMensajeSimple(std::string,Conversacion*)=0;
    virtual void enviarMensajeVideo(std::string,int,Conversacion*)=0;
private:

};

#endif /* ICONTROLADORM_H */

