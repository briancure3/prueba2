#include "DtConversacion.h"

DtConversacion::DtConversacion() {
}

DtConversacion::DtConversacion(const DtConversacion& orig) {
}

DtConversacion::~DtConversacion() {
}

std::string DtConversacion::getTelefono()
{
    return this->telefono;
}
bool DtConversacion::getEsGrupal()
{ 
    return this->esGrupal;
}

std::string DtConversacion::getNombre()
{
    return this->nombre;
}
std::string DtConversacion::getNombreGrupo()
{
    return this->nomGrupo;
}

void DtConversacion::setEsGrupal(bool es)
{
    (*this).esGrupal=es;
}

void DtConversacion::setTelefono(std::string tel)
{
    (*this).telefono=tel;
}

void DtConversacion::setNombre(std::string nom)
{
    (*this).nombre=nom;
}

void DtConversacion::setNombreGrupo(std::string nomg)
{
    (*this).nomGrupo=nomg;
}
void DtConversacion::setId(int id1)
{
    (*this).id=id1;
}
int DtConversacion::getId()
{
    return this->id;
}