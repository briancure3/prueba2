#ifndef SESION_H
#define SESION_H
using namespace std;
#include <stdlib.h>
#include <iostream>
#include "Usuario.h"

class Sesion {
private:
    Sesion();
    static Sesion* unica_instancia;
    Usuario *user;
public:
    Sesion(const Sesion& orig);
    virtual ~Sesion();
    static Sesion *getInstance()
    {
        if(unica_instancia == NULL)
        {
            unica_instancia=new Sesion();
        }
        return unica_instancia;
    }
    Usuario* getU();
    string buscarTel(std::string);
    void cerrarSesion();
    void setU(Usuario*);
};

#endif /* SESION_H */