#ifndef ICONTROLADORG_H
#define ICONTROLADORG_H
#include "DtContacto.h"
#include <set>

class IControladorG {
public:
    IControladorG();
    IControladorG(const IControladorG& orig);
    virtual ~IControladorG();
    virtual void setNoElegidos(set<DtContacto*>)=0;
    virtual set<DtContacto*> listarElegidos()=0;
    virtual set<DtContacto*> listarNoElegidos()=0;
    virtual void agregarParticipante(string)=0;
    virtual void borrarParticipante(string)=0;
    virtual void darAltaGrupo(string, string)=0;
private:

};

#endif /* ICONTROLADORG_H */
