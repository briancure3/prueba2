#include <set>
#include "Grupo.h"

Grupo::Grupo() {
}

Grupo::Grupo(const Grupo& orig) {
}

Grupo::~Grupo() {
}

string Grupo::getNombre() {
    return this->nombre;
}

void Grupo::setNombre(string nom) {
    (*this).nombre = nom;
}

string Grupo::getUrlImagen() {
    return this->urlImagen;
}

void Grupo::setUrlImagen(string url) {
    (*this).urlImagen = url;
}

Fecha Grupo::getFechaCreacion() {
    return this->fecha_creacion;
}

void Grupo::setFechaCreacion(Fecha fech) {
    (*this).fecha_creacion = fech;
}

string Grupo::getHoraCreacion() {
    return this->hora_creacion;
}

void Grupo::setHoraCreacion(string hor) {
    (*this).hora_creacion = hor;
}

void Grupo::setDatoRegistro(Datos_Miembro* dm) {
    it=listaDatosMiembro.begin();
    listaDatosMiembro.insert(it, dm);
}