#ifndef DTCONVERSACION_H
#define DTCONVERSACION_H
#include <stdlib.h>
#include <iostream>

class DtConversacion {
public:
    DtConversacion();
    DtConversacion(const DtConversacion& orig);
    virtual ~DtConversacion();
    bool getEsGrupal();
    std::string getTelefono();
    std::string getNombre();
    std::string getNombreGrupo();
    int getId();
    void setId(int);
    void setEsGrupal(bool);
    void setTelefono(std::string);
    void setNombre(std::string);
    void setNombreGrupo(std::string);
private:
    int id;
    bool esGrupal;
    std::string telefono;
    std::string nombre;
    std::string nomGrupo;
};

#endif /* DTCONVERSACION_H */

