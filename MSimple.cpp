#include "MSimple.h"

MSimple::MSimple():Mensaje() {
}

MSimple::MSimple(const MSimple& orig) {
}

MSimple::~MSimple() {
}

string MSimple::getTexto()
{
    return this->texto;
}

void MSimple::setTexto(string txt)
{
    (*this).texto=txt;
}

void MSimple::setReceptores(set<Usuario*> rec)
{
    set<Usuario*>::iterator it5;
    for(it5=rec.begin();it5!=rec.end(); ++it5)
    {
        DatosVisto *dv_1= new DatosVisto;
        dv_1->setReceptor(*it5);
        (*this).dv.insert(dv_1);
    }
}

void MSimple::setReceptor(Usuario* rec){
    DatosVisto *dv_2= new DatosVisto;
    dv_2->setReceptor(rec);
}