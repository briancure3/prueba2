#ifndef USUARIO_H
#define USUARIO_H
using namespace std;
#include <stdlib.h>
#include <iostream>
#include "Fecha.h"
#include "DtUsuario.h"
#include "Grupo.h"
#include "Conversacion.h"
#include <set>
#include "DtConversacion.h"

class Usuario  {
private:
    string telefono;
    string nombre;
    Fecha fecha_de_registro;
    string url;
    string descripcion;
    string ultima_vez_conectado;
    set<Usuario*>listaContactos;
    set<Usuario*>::iterator it, it2;
    set<Conversacion*>listaConversaciones;
    set<Conversacion*>::iterator itc, it2c;
    set<Grupo*>listaGrupos;
    set<Grupo*>::iterator it3;
public:
    Usuario();
    Usuario(const Usuario& orig);
    virtual ~Usuario();
    string getTelefono();
    string getNombre();
    string getUrl();
    string getDescripcion();
    string getUltima_vez_conectado();
    set<Usuario*> obtenerContactos();
    void setTelefono(string);
    void setNombre(string);
    void setUrl(string);
    void setDescripcion(string);
    void setUltima_vez_conectado(string);
    void confirmar(DtUsuario*);
    bool esContacto(string);
    void setGrupo(Grupo*);
    Conversacion* altaConversacion(std::string);
    std::set<DtConversacion*> listarConversaciones(std::string);
    Conversacion* seleccionarConversacion(int);


};

#endif /* USUARIO_H */

