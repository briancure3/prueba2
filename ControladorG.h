#ifndef CONTROLADORG_H
#define CONTROLADORG_H
#include "IControladorG.h"
#include "DtContacto.h"
#include "Fecha.h"
#include "FechaActual.h"
#include "Sesion.h"
#include "Grupo.h"
#include "Datos_Miembro.h"
#include <set>

class ControladorG : public IControladorG {
public:
    ControladorG();
    ControladorG(const ControladorG& orig);
    virtual ~ControladorG();
    void setNoElegidos(set<DtContacto*>);
    set<DtContacto*> listarElegidos();
    set<DtContacto*> listarNoElegidos();
    void agregarParticipante(string);
    void borrarParticipante(string);
    void darAltaGrupo(string, string);
private:
    static set<DtContacto*> Elegidos;
    static set<DtContacto*> NoElegidos;
    set<DtContacto*>::iterator it;
    set<DtContacto*>::iterator it2;
};

#endif /* CONTROLADORG_H */
