/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ControladorM.h
 * Author: brian
 *
 * Created on 6 de julio de 2018, 12:07 AM
 */

#ifndef CONTROLADORM_H
#define CONTROLADORM_H
#include "IControladorM.h"
#include "Sesion.h"

class ControladorM : public IControladorM{
public:
    ControladorM();
    ControladorM(const ControladorM& orig);
    virtual ~ControladorM();
    void enviarMensajeSimple(std::string, Conversacion* c);
    void enviarMensajeVideo(std::string,int, Conversacion* c);
private:

};

#endif /* CONTROLADORM_H */

