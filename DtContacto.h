#ifndef DTCONTACTO_H
#define DTCONTACTO_H
#include <string>
#include <iostream>
#include <stdlib.h>
using namespace std;

class DtContacto {
public:
    DtContacto();
    DtContacto(const DtContacto& orig);
    virtual ~DtContacto();
    string getTelefono();
    string getNombre();
    void setTelefono(string);
    void setNombre(string);
private:
    string telefono;
    string nombre;
};

#endif /* DTCONTACTO_H */
