#include "ControladorM.h"

ControladorM::ControladorM() {
}

ControladorM::ControladorM(const ControladorM& orig) {
}

ControladorM::~ControladorM() {
}

void ControladorM::enviarMensajeSimple(std::string msj, Conversacion* c)
{
    Sesion* login= Sesion::getInstance();
    c->enviarMensajeSimple(msj,login->getU());
}

void ControladorM::enviarMensajeVideo(std::string URL,int Duracion, Conversacion* c)
{
    Sesion* login= Sesion::getInstance();
    c->enviarMensajeVideo(URL,Duracion,login->getU());
}