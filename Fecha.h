#ifndef FECHA_H
#define FECHA_H
#include <iostream>
using namespace std;

class Fecha {
public:
    Fecha();
    Fecha(int,int,int);
    bool operator==(Fecha& valor)const;
    Fecha(const Fecha& orig);
    virtual ~Fecha();
    void setDia(int);
    int getDia();
    void setMes(int);
    int getMes();
    void setAnio(int);
    int getAnio();
private:
    int dia;
    int mes;
    int anio;
};

#endif /* FECHA_H */