#include "Sesion.h"

Sesion* Sesion::unica_instancia = NULL;

Sesion::Sesion() {
}
    
Sesion::Sesion(const Sesion& orig) {
}

Sesion::~Sesion() {
}

Usuario* Sesion::getU(){
    return this->user;
}

void Sesion::setU(Usuario* us){
    (*this).user=us;
}

string Sesion::buscarTel(std::string tel)
{
    if((*this).user==NULL)
    {
    return "Es nulo";
    }
    else
    {
        if((*this).user->getTelefono()==tel)
        {
            return "Salir";
        }
        else
        {
           return "Cerrar"; 
        }
    return "No nulo";
    }
}

void Sesion::cerrarSesion()
{
    if((*this).user==NULL)
    {
        std::cout << "No se puede cerrar sesion, porque no hay sesion iniciada" << std::endl;
    }
    else
    {
        std::cout << "Se cerro sesion correctamente " << std::endl;
        (*this).user=NULL;
    }
    
}