#ifndef CONVERSACION_H
#define CONVERSACION_H

#include <stdlib.h>
#include <iostream>
#include <set>
#include "Mensaje.h"
#include "MSimple.h"
#include "MVideo.h"


class Usuario;
class Conversacion {
public:
    Conversacion();
    Conversacion(const Conversacion& orig);
    virtual ~Conversacion();
    void setId(int);
    void setEsGrupal(bool);
    void setEsActiva(bool);
    int getId();
    bool getEsGrupal();
    bool getEsActiva();
    void setUsuarios(Usuario*, Usuario*);
    Usuario* getU();
    void enviarMensajeSimple(std::string, Usuario*);
    void enviarMensajeVideo(std::string,int, Usuario*);
private:
    unsigned int id;
    static unsigned int id_counter;
    bool esActiva;
    bool esGrupal;
    Usuario* emisor;
    Usuario* receptor;
    std::set <Usuario *> receptoresG;
    std::set <Usuario *>::iterator it;
    std::set<Mensaje *> mensajes;
};
#include "Usuario.h"
#endif /* CONVERSACION_H */

