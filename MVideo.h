

#ifndef MVIDEO_H
#define MVIDEO_H
#include "Mensaje.h"
#include <set>
#include "Usuario.h"

class MVideo: public Mensaje {
public:
    MVideo();
    MVideo(const MVideo& orig);
    virtual ~MVideo();
    string getURL();
    string getDuracion();
    void setURL(string);
    void setDuracion(int);
    void setReceptores(set<Usuario*>);
    void setReceptor(Usuario*);
private:
    string URL;
    int Duracion;
};

#endif /* MVIDEO_H */

