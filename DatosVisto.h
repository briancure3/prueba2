#ifndef DATOSVISTO_H
#define DATOSVISTO_H
#include "Usuario.h"

class DatosVisto {
public:
    DatosVisto();
    DatosVisto(const DatosVisto& orig);
    virtual ~DatosVisto();
    void setReceptor(Usuario*);
private:
    Usuario* receptorM;
};
#endif /* DATOSVISTO_H */

