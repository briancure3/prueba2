#include "Fabrica.h"
Fabrica* Fabrica::unica_instancia = NULL;

Fabrica::Fabrica() {
}

Fabrica::Fabrica(const Fabrica& orig) {
}

Fabrica::~Fabrica() {
}
IControladorU* Fabrica::getInterfazU()
{
    return new ControladorU();
}
IControladorC* Fabrica::getInterfazC()
{
    return new ControladorC();
}
IControladorG* Fabrica::getInterfazG()
{
    return new ControladorG();
}
IControladorM* Fabrica::getInterfazM(){
    return new ControladorM();
}
void Fabrica::borrarInterfaz(IControladorU* interf)
{
    delete interf;
}

void Fabrica::borrarInterfazG(IControladorG* interf)
{
    delete interf;
}
