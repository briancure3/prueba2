#include "ControladorG.h"
#include "Grupo.h"
#include <string>
#include <sstream>

set<DtContacto*> ControladorG::Elegidos;
set<DtContacto*> ControladorG::NoElegidos;

ControladorG::ControladorG() {
}

ControladorG::ControladorG(const ControladorG& orig) {
}

ControladorG::~ControladorG() {
}

void ControladorG::setNoElegidos(set<DtContacto*> cont){
    NoElegidos=cont;
}

set<DtContacto*> ControladorG::listarElegidos(){
    return Elegidos;
}

set<DtContacto*> ControladorG::listarNoElegidos(){
    return NoElegidos;
}

void ControladorG::agregarParticipante(string tel){
    it = Elegidos.begin();
    it2 = NoElegidos.begin();
    while((*it2)->getTelefono()!=tel){
        it2++;
    }
    Elegidos.insert(it, *it2);
    NoElegidos.erase(it2);
}

void ControladorG::borrarParticipante(string tel){
    it = Elegidos.begin();
    it2 = NoElegidos.begin();
    while((*it)->getTelefono()!=tel){
        it++;
    }
    NoElegidos.insert(it2, *it);
    Elegidos.erase(it);
}

void ControladorG::darAltaGrupo(string nombre, string URL){
    Sesion *log= Sesion::getInstance();
    FechaActual *fecA= FechaActual::getInstance();
    Usuario* user=log->getU();
    set<Usuario*> contactosU=user->obtenerContactos();
    set<Usuario*>::iterator it3=contactosU.begin();
    it = Elegidos.begin();
    Fecha fech;
    std::string horaA, minA;
    std::stringstream porHora, porMin;
    Grupo *grup= new Grupo;
    fech.setDia(fecA->getDia());
    fech.setMes(fecA->getMes());
    fech.setAnio(fecA->getAnio());
    porHora << fecA->getHora();
    horaA=porHora.str();
    porMin << fecA->getMinutos();
    minA=porMin.str();
    std::string hor = horaA + ":" + minA;
    
    grup->setNombre(nombre);
    grup->setUrlImagen(URL);
    grup->setFechaCreacion(fech);
    grup->setHoraCreacion(hor);
    
    for(it=Elegidos.begin(); it!=Elegidos.end();++it){
        for(it3=contactosU.begin(); it3!=contactosU.end();++it3){
            if((*it)->getTelefono()==(*it3)->getTelefono()){
                Datos_Miembro* dtm=new Datos_Miembro;
                dtm->setFechaAgregado(fech);
                dtm->setHoraAgregado(hor);
                dtm->setMiembro(*it3);
                grup->setDatoRegistro(dtm);
                delete dtm;
            }
        }
    }
    
    Datos_Miembro* dmUser=new Datos_Miembro;
    dmUser->setFechaAgregado(fech);
    dmUser->setHoraAgregado(hor);
    dmUser->setMiembro(user);
    grup->setDatoRegistro(dmUser);
    delete dmUser;
    
    user->setGrupo(grup);
    Elegidos.clear();
    NoElegidos.clear();
    std::cout<<"Se creo el grupo correctamente\n"<<std::endl;
}