#ifndef DATOS_MIEMBRO_H
#define DATOS_MIEMBRO_H
using namespace std;
#include <stdlib.h>
#include <iostream>
#include "Fecha.h"
#include "Usuario.h"
#include "Datos_Miembro.h"

class Datos_Miembro {
public:
    Datos_Miembro();
    Datos_Miembro(const Datos_Miembro& orig);
    virtual ~Datos_Miembro();
    string getHoraAgregado();
    void setHoraAgregado(string);
    Fecha getFechaAgregado();
    void setFechaAgregado(Fecha);
    void setMiembro(Usuario*);
private:
    string horaAgregado;
    Fecha fechaAgregado;
    Usuario* miembro;
};

#endif /* DATOS_MIEMBRO_H */
