#include "DtUsuario.h"

DtUsuario::DtUsuario() {
}/*
DtUsuario::DtUsuario(std::string tel, std::string nom, std::string url, std::string desc, std::string ult) {
    (*this).telefono=tel;
    (*this).nombre=nom;
    (*this).url=url;
    (*this).descripcion=desc;
    (*this).ultima_vez_conectado=ult;
}*/

DtUsuario::DtUsuario(const DtUsuario& orig) {
}

DtUsuario::~DtUsuario() {
}

std::string DtUsuario::getTelefono() {
    return this->telefono;
}

std::string DtUsuario::getNombre() {
    return this->nombre;
}

std::string DtUsuario::getUrl() {
    return this->url;
}

std::string DtUsuario::getDescripcion() {
    return this->descripcion;
}

std::string DtUsuario::getUltima_vez_conectado() {
    return this->ultima_vez_conectado;
}
void DtUsuario::setTelefono(string tel) {
    (*this).telefono = tel;
}

void DtUsuario::setNombre(string nom) {
    (*this).nombre = nom;
}

void DtUsuario::setUrl(string ur) {
    (*this).url = ur;
}

void DtUsuario::setDescripcion(string desc) {
    (*this).descripcion = desc;
}

void DtUsuario::setUltima_vez_conectado(string ult) {
    (*this).ultima_vez_conectado = ult;
}
