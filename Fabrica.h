/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#ifndef FABRICA_H
#define FABRICA_H
#include <stdlib.h>
#include <iostream>
#include "IControladorU.h"
#include "IControladorC.h"
#include "IControladorG.h"
#include "ControladorU.h"
#include "ControladorC.h"
#include "ControladorG.h"
#include "IControladorM.h"
#include "ControladorM.h"
class Fabrica {
public:
    Fabrica(const Fabrica& orig);
    virtual ~Fabrica();
    static Fabrica *getInstance()
    {
        if(unica_instancia == NULL)
        {
            unica_instancia=new Fabrica();
        }
        return unica_instancia;
    }
    IControladorU *getInterfazU();
    IControladorC *getInterfazC();
    IControladorG *getInterfazG();
    IControladorM *getInterfazM();
    void borrarInterfazG(IControladorG*);
    void borrarInterfaz(IControladorU*);
    
private:
    Fabrica();
    static Fabrica* unica_instancia;
};

#endif /* FABRICA_H */

