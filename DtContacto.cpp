#include "DtContacto.h"

DtContacto::DtContacto() {
}

DtContacto::DtContacto(const DtContacto& orig) {
}

DtContacto::~DtContacto() {
}

string DtContacto::getTelefono() {
    return this->telefono;
}

string DtContacto::getNombre() {
    return this->nombre;
}

void DtContacto::setTelefono(string tel) {
    (*this).telefono = tel;
}

void DtContacto::setNombre(string nom) {
    (*this).nombre = nom;
}