#include "Usuario.h"

Usuario::Usuario() {
}

Usuario::Usuario(const Usuario& orig) {
}

Usuario::~Usuario() {
}

string Usuario::getTelefono() {
    return this->telefono;
}

string Usuario::getNombre() {
    return this->nombre;
}

string Usuario::getUrl() {
    return this->url;
}

string Usuario::getDescripcion() {
    return this->descripcion;
}

string Usuario::getUltima_vez_conectado() {
    return this->ultima_vez_conectado;
}

void Usuario::setTelefono(string tel) {
    (*this).telefono = tel;
}

void Usuario::setNombre(string nom) {
    (*this).nombre = nom;
}

void Usuario::setUrl(string ur) {
    (*this).url = ur;
}

void Usuario::setDescripcion(string desc) {
    (*this).descripcion = desc;
}

void Usuario::setUltima_vez_conectado(string ult) {
    (*this).ultima_vez_conectado = ult;
}

set<Usuario*> Usuario::obtenerContactos()
{
  /*Usuario *u1  =new Usuario;
  u1->setTelefono("15");
  u1->setNombre("Alberto Melendez");
  u1->setUrl("www.pagina.com");
  listaContactos.insert(u1);*/
  return (*this).listaContactos;   
}

void Usuario::confirmar(DtUsuario* dtU)
{
     Usuario *u1 = new Usuario;
    u1->setTelefono(dtU->getTelefono());
    u1->setNombre(dtU->getNombre());
    u1->setUrl(dtU->getUrl());
    u1->setDescripcion(dtU->getDescripcion());
    u1->setUltima_vez_conectado(dtU->getUltima_vez_conectado());
    listaContactos.insert(u1);
}
bool Usuario::esContacto(string tel)
{
    bool esC = false;
    if(listaContactos.empty()==false)
    {
    for (it=listaContactos.begin();it!=listaContactos.end();++it)
        {
        if((*it)->getTelefono()==tel)
        {
             esC=true;
             it2=it; 
             it=listaContactos.end();
             --it;
        }
        }
    }
    return esC;
}

Conversacion* Usuario::altaConversacion(std::string tel)
{
    bool esC = false;
    if(listaContactos.empty()==false)
    {
    for (it=listaContactos.begin();it!=listaContactos.end();++it)
        {
        if((*it)->getTelefono()==tel)
        {
             esC=true;
             it2=it; 
             it=listaContactos.end();
             --it;
        }
        }
    }
    if(esC)
    {
        Conversacion* c1= new Conversacion;
        c1->setUsuarios(this, *it2);
        c1->setEsActiva(true);
        c1->setEsGrupal(false);
        listaConversaciones.insert(c1);
        return c1;
    }
    else
    {
        std::cout<< "No es contacto" << std::endl;
        return NULL;
    }
    return NULL;
}

std::set<DtConversacion*> Usuario::listarConversaciones(std::string opc)
{
    set<DtConversacion*> listaConv;
    set<DtConversacion*>::iterator dtIt;
    if(listaConversaciones.empty()==false)
    {
    for (itc=listaConversaciones.begin();itc!=listaConversaciones.end();++itc)
    {
        if(opc=="Activas")
        {
          if((*itc)->getEsActiva())
            {
                DtConversacion *dt1= new DtConversacion;
                dt1->setEsGrupal((*itc)->getEsGrupal());
                dt1->setId((*itc)->getId());
                if(dt1->getEsGrupal()==false)
                {
                dt1->setNombre((*itc)->getU()->getNombre());
                dt1->setTelefono((*itc)->getU()->getTelefono());
                }
                else
                {
               //ACA VA SI ES GRUPAL 
                }
                listaConv.insert(dt1);
            }
        }
        else
        {
           if((*itc)->getEsActiva()==false)
            {
                DtConversacion *dt1= new DtConversacion;
                dt1->setEsGrupal((*itc)->getEsGrupal());
                dt1->setId((*itc)->getId());
                if(dt1->getEsGrupal()==false)
                {
                dt1->setNombre((*itc)->getU()->getNombre());
                dt1->setTelefono((*itc)->getU()->getTelefono());
                }
                else
                {
               //ACA VA SI ES GRUPAL 
                }
                listaConv.insert(dt1);
            } 
        }
    }  
    }     
        
   
    return listaConv;
}

void Usuario::setGrupo(Grupo* gru) {
    it3=listaGrupos.begin();
    listaGrupos.insert(it3, gru);
}

Conversacion* Usuario::seleccionarConversacion(int idC)
{
    set<Conversacion*>::iterator itSC;
    for(itSC=listaConversaciones.begin();itSC!=listaConversaciones.end();++itSC)
    {
    if(idC==((*itSC)->getId()))
    {
        return *itSC;
    }
    }
    return NULL;
}