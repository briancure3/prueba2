#ifndef GRUPO_H
#define GRUPO_H
using namespace std;
#include <stdlib.h>
#include <iostream>
#include "Fecha.h"
//#include "Datos_Miembro.h"
#include <set>
#include <iterator>
class Datos_Miembro;

class Grupo {
public:
    Grupo();
    Grupo(const Grupo& orig);
    virtual ~Grupo();
    string getNombre();
    void setNombre(string);
    string getUrlImagen();
    void setUrlImagen(string);
    Fecha getFechaCreacion();
    void setFechaCreacion(Fecha);
    string getHoraCreacion();
    void setHoraCreacion(string);
    void setDatoRegistro(Datos_Miembro*);
private:
    string nombre;
    string urlImagen;
    Fecha fecha_creacion;
    string hora_creacion;
    set<Datos_Miembro*>listaDatosMiembro;
    set<Datos_Miembro*>::iterator it;
};

#endif /* GRUPO_H */
