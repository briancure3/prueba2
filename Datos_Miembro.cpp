#include "Datos_Miembro.h"

Datos_Miembro::Datos_Miembro() {
}

Datos_Miembro::Datos_Miembro(const Datos_Miembro& orig) {
}

Datos_Miembro::~Datos_Miembro() {
}

string Datos_Miembro::getHoraAgregado() {
    return this->horaAgregado;
}

void Datos_Miembro::setHoraAgregado(string hor) {
    (*this).horaAgregado = hor;
}

Fecha Datos_Miembro::getFechaAgregado() {
    return this->fechaAgregado;
}

void Datos_Miembro::setFechaAgregado(Fecha fech) {
    (*this).fechaAgregado = fech;
}

void Datos_Miembro::setMiembro(Usuario* m){
    (*this).miembro=m;
}