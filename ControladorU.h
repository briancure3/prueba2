//esto es un comentario de prueba
#ifndef CONTROLADORU_H
#define CONTROLADORU_H

#include "IControladorU.h"
#include "Usuario.h"
#include "DtUsuario.h"
#include "Sesion.h"
#include <vector>
#include <string>
#include <set>

class ControladorU : public IControladorU {
public:
    ControladorU();
    ControladorU(const ControladorU& orig);
    virtual ~ControladorU();
    string ingresarNumero(std::string);
    void darAltaNumero(std::string, std::string, std::string, std::string);
    void cerrarSesion();
    set<Usuario*> obtenerContactos();
    DtUsuario* mostrarContacto(std::string);
    void confirmar(DtUsuario*);
private:
    static set<Usuario*> listaUsuarios; 
    set<Usuario*>::iterator it, it2;
};

#endif /* CONTROLADORU_H */

