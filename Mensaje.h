#ifndef MENSAJE_H
#define MENSAJE_H
#include <string>
#include "Fecha.h"

#include <set>


class Conversacion;
class Usuario;
class DatosVisto;
class Mensaje {
public:
    Mensaje();
    Mensaje(const Mensaje& orig);
    virtual ~Mensaje();
    int getCodigo();
    Fecha getFecha();
    string getHora();
    void setCodigo(int);
    void setFecha(Fecha);
    void setHora(string);
    virtual void setTexto(string)=0;
    virtual string getTexto()=0;
    virtual void setURL(string)=0;
    virtual string getURL()=0;
    virtual void setDuracion(int)=0;
    virtual string getDuracion()=0;
    virtual void setReceptores(set<Usuario*>)=0;
    virtual void setReceptor(Usuario*)=0;
private:
    int codigo;
    Fecha fecha;
    string hora;
protected:
    set<DatosVisto*> dv;
};
#include "Conversacion.h"
#include "Usuario.h"
#include "DatosVisto.h"
#endif /* MENSAJE_H */

