#ifndef ICONTROLADORU_H
#define ICONTROLADORU_H
#include <string>
#include <vector>
#include "DtUsuario.h"
#include "Usuario.h"
#include <set>

//esto es un comentario agregado


class IControladorU {
public:
    IControladorU();
    IControladorU(const IControladorU& orig);
    virtual ~IControladorU();
    virtual std::string ingresarNumero(std::string)= 0;
    virtual void darAltaNumero(std::string, std::string, std::string, std::string)=0;
    virtual void cerrarSesion()=0;
    virtual set <Usuario*> obtenerContactos()=0;
    virtual DtUsuario* mostrarContacto(std::string)=0;
    virtual void confirmar(DtUsuario*)=0;
private:

};

#endif /* ICONTROLADORU_H */
