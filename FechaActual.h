#ifndef FECHAACTUAL_H
#define FECHAACTUAL_H
using namespace std;
#include <stdlib.h>
#include <iostream>

class FechaActual {
private:
    FechaActual();
    FechaActual(int,int,int,int,int);
    static FechaActual* unica_instancia;
    int dia;
    int mes;
    int anio;
    int hora;
    int minuto;
public:
    FechaActual(const FechaActual& orig);
    virtual ~FechaActual();
    static FechaActual *getInstance()
    {
        if(unica_instancia == NULL)
        {
            unica_instancia=new FechaActual(0,0,0,0,0);
        }
        return unica_instancia;
    }
    void modFechAct(int,int,int,int,int);
    void verFechAct();
    int getDia();
    int getMes();
    int getAnio();
    int getHora();
    int getMinutos();
};

#endif /* FECHAACTUAL_H */
