#include "FechaActual.h"

FechaActual* FechaActual::unica_instancia = NULL;

FechaActual::FechaActual() {
}

FechaActual::FechaActual(int d,int m,int a,int h,int min) {
    (*this).dia=d;
    (*this).mes=m;
    (*this).anio=a;
    (*this).hora=h;
    (*this).minuto=min;
}

FechaActual::FechaActual(const FechaActual& orig) {
}

FechaActual::~FechaActual() {
}

void FechaActual::modFechAct(int d,int m,int a,int h,int min){
    (*this).dia=d;
    (*this).mes=m;
    (*this).anio=a;
    (*this).hora=h;
    (*this).minuto=min;
}

void FechaActual::verFechAct(){
    std::cout<<"["<<this->dia<<"/"<<this->mes<<"/"<<this->anio<<" "<<this->hora<<":"<<this->minuto<<"]"<<std::endl;
}

int FechaActual::getDia(){
    return this->dia;
}

int FechaActual::getMes(){
    return this->mes;
}

int FechaActual::getAnio(){
    return this->anio;
}

int FechaActual::getHora(){
    return this->hora;
}

int FechaActual::getMinutos(){
    return this->minuto;
}