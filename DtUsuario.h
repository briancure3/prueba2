
#ifndef DTUSUARIO_H
#define DTUSUARIO_H
#include <string>
#include <iostream>
#include <stdlib.h>
#include "Fecha.h"


class DtUsuario {
public:
    DtUsuario();
    //DtUsuario(std::string,std:: string, std::string, std::string, std::string);
    DtUsuario(const DtUsuario& orig);
    virtual ~DtUsuario();
    std::string getTelefono();
    std::string getNombre();
    std::string getUrl();
    std::string getDescripcion();
    std::string getUltima_vez_conectado();
    void setTelefono(string);
    void setNombre(string);
    void setUrl(string);
    void setDescripcion(string);
    void setUltima_vez_conectado(string);

private:
    std::string telefono;
    std::string nombre;
    Fecha fecha_de_registro;
    std::string url;
    std::string descripcion;
    std::string ultima_vez_conectado;

};

#endif /* DTUSUARIO_H */

