#include "ControladorU.h"

set<Usuario*> ControladorU::listaUsuarios;

ControladorU::ControladorU() {
}

ControladorU::ControladorU(const ControladorU& orig) {
}

ControladorU::~ControladorU() {
}
string ControladorU::ingresarNumero(std::string tel)
{
    Usuario* u1= new Usuario;
    Usuario* u2= new Usuario;
    Usuario* u3= new Usuario;
    Usuario* u4= new Usuario;
    Usuario* u5= new Usuario;
    u2->setTelefono("10"); u2->setNombre("Alberto");
    u1->setTelefono("5");u1->setNombre("Jose");
    u3->setTelefono("15");u3->setNombre("Felipe");
    u4->setTelefono("20");u4->setNombre("Maria");
    u5->setTelefono("25");u5->setNombre("Karen");
    listaUsuarios.insert(u1);
    listaUsuarios.insert(u2);
    listaUsuarios.insert(u3);
    listaUsuarios.insert(u4);
    listaUsuarios.insert(u5);
    if(listaUsuarios.empty())
    { 
        return "No esta";
    }
    else
    {
        bool esta = false;
        for (it=listaUsuarios.begin();it!=listaUsuarios.end();++it)
        {
        if((*it)->getTelefono()==tel)
        {
             esta=true;
             it2=it;
             it=listaUsuarios.end();
             --it;
        }
        }
        Sesion *login = Sesion::getInstance();
            string caso=login->buscarTel(tel);
            if(caso=="Es nulo" && esta==false)
            {
                return "No esta";
            }
            if(caso=="Es nulo")
            {
            login->setU((*it2));            
            return "Se logueo"; 
            }
            if(caso=="Salir")
            {
                return "Salir";
            }
            if(caso=="Cerrar")
            {
                return "Cerrar";
            }  
    }
    return "Salir";
}
void ControladorU::darAltaNumero(std::string t, std::string n, std::string ur, std::string d)
{
  Usuario *u1 = new Usuario;
  u1->setTelefono(t);
  u1->setNombre(n);
  u1->setUrl(ur);
  u1->setDescripcion(d);
  listaUsuarios.insert(u1);
  Sesion *login = Sesion::getInstance();
  login->setU(u1);
}

void ControladorU::cerrarSesion()
{
  Sesion *login = Sesion::getInstance();
  login->cerrarSesion();
}

set<Usuario*> ControladorU::obtenerContactos()
{
    Sesion *login= Sesion::getInstance();
    return login->getU()->obtenerContactos();
    
}

DtUsuario* ControladorU::mostrarContacto(std::string tel){
    bool existe = false;
    for (it=listaUsuarios.begin();it!=listaUsuarios.end();++it)
        {
        if((*it)->getTelefono()==tel)
        {
             existe=true;
             it2=it; 
             it=listaUsuarios.end();
             --it;
        }
        }
    if(existe==false)
    {
        std::cout<< " No existe un usuario registrado con ese telefono " << std::endl;
        return NULL;
    }
    Sesion *login= Sesion::getInstance();
    bool esContacto=login->getU()->esContacto(tel);
    if(esContacto==true)
    {
        std::cout << "El usuario a agregar ya es contacto" << std::endl;
        return NULL;
    }
    else
    {
       DtUsuario *dtU = new DtUsuario;
       dtU->setTelefono((*it2)->getTelefono());
       dtU->setNombre((*it2)->getNombre());
       dtU->setUrl((*it2)->getUrl());
       dtU->setDescripcion((*it2)->getDescripcion());
       dtU->setUltima_vez_conectado((*it2)->getUltima_vez_conectado());
       return dtU;
    }
}

void ControladorU::confirmar(DtUsuario* dtU)
{
    Sesion *login= Sesion::getInstance();
    login->getU()->confirmar(dtU);
}